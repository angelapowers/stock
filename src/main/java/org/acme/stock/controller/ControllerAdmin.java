package org.acme.stock.controller;


import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.acme.stock.models.ShopResponse;
import org.acme.stock.services.ServicesAdmin;

@Path("/stock/V2")
public class ControllerAdmin {

	@Inject
	ServicesAdmin servicesAdmin;
	
	@GET
	@Path("/shop")
	public Response getShops(@QueryParam("pageSize")Integer pageSize){
		return servicesAdmin.getShopList(pageSize);
	}
	
	@POST
	@Path("/shop")
	public Response addShopRes(ShopResponse shopAdd) {
		return servicesAdmin.addShop(shopAdd);
	}
	
	@GET
	@Path("/shop/{t1_storeid}")
	public Response findTidShop(@PathParam ( "t1_storeid" ) Integer t1_storeid) {
		return servicesAdmin.findStoreid(t1_storeid);
	}
	
	@PUT
	@Path("/shop/{t1_storeid}")
	public Response upDate (@PathParam ( "t1_storeid" ) Integer t1_storeid, ShopResponse up) {
		return servicesAdmin.updateStoreid(t1_storeid, up);
	}
	
	@PATCH
	@Path("/shop/{t1_storeid}")
	public Response upPatchDate (@PathParam ( "t1_storeid" ) Integer t1_storeid, Map<String, Object> fields) {
		return servicesAdmin.upPatchStore(t1_storeid, fields);
	}
	
}