package org.acme.stock.controller;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.acme.stock.models.ListProductRequest;
import org.acme.stock.services.ServicesFront;

@Path("/stock/V2")
public class ControllerFront {
	
	@Inject
	ServicesFront servicesFront;
	
	@POST
	@Path("/product/list")
	public Response getSku (ListProductRequest add) {
		return servicesFront.getProductSku(add);
	} 

}
