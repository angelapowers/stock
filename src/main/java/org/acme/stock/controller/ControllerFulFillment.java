package org.acme.stock.controller;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.acme.stock.models.FulfillmentRequest;
import org.acme.stock.models.SetFulfillmentRequest;
import org.acme.stock.services.ServicesFulFilment;

@Path("/stock/V2")
public class ControllerFulFillment {
	
	@Inject
	ServicesFulFilment servicesFulFilment;
	
	@POST
	@Path("/product/add-fulfillment")	
	public Response addFulfillment (List<FulfillmentRequest> addFull) {
		return servicesFulFilment.addFullfilment(addFull); 
	}
	
	@POST
	@Path("/product/set-fulfillment")
	public Response setFulfillmet(SetFulfillmentRequest set) {
		return servicesFulFilment.setFulfillment(set);
	}
}