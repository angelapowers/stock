package org.acme.stock.models;

import java.time.LocalDateTime;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class BasicMetaData {

	private Boolean is_error;
    private String status;
    private Integer http_code;
    private LocalDateTime date_time;
    private String message;
    
    
    public BasicMetaData InitialValue(BasicMetaData setdata,Boolean create) {
    	BasicMetaData change = setdata;
    	if (create == true) {
    		change.setIs_error(true); 
			change.setHttp_code(404);
			change.setDate_time(LocalDateTime.now());
			change.setMessage("Not Found");
			change.setStatus("faile");
    	}else {
    		change.setIs_error(false); 
			change.setHttp_code(200);
			change.setDate_time(LocalDateTime.now());
			change.setMessage("ok");
			change.setStatus("success");
    	}
    	return change;
    }
}
