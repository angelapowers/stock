package org.acme.stock.models;

import java.util.List;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@Data
@RegisterForReflection
public class ListProductRequest {
	
	private Integer t1_storeid;
	private List<String> sku;

}
