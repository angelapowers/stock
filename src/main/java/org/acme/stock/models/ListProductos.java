package org.acme.stock.models;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class ListProductos {

	
	private String description;
	private Integer id_fulfillment;
	private String sku;
	private String ean;
	private Integer stock_one;
	private Integer stock;
	private Integer hold;
	private Integer available;
	private Integer t1_storeid; 
	
	public ListProductos initValues(ListProductos initial, Products param) {
		initial.setSku(param.getSku());
		initial.setId_fulfillment(param.getId_fulfillment()); 
		initial.setStock(param.getStock());
		initial.setT1_storeid(param.getT1_storeid());
		initial.setEan(param.getEan());
		initial.setStock_one(param.getStock());
		initial.setDescription(param.getNombre());
		initial.setHold(param.getHold());
		initial.setAvailable(param.getStock() - param.getHold());
		return initial;
	}
	
}
