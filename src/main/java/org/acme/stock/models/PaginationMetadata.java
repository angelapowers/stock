package org.acme.stock.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Data;

@Data
public class PaginationMetadata {
	@Min(0)
	private Long total_count;
	@Min(1)
	private Integer page;
	@Min(1)
	@Max(50)
	private Integer page_size;
}
