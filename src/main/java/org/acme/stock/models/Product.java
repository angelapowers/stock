package org.acme.stock.models;

import lombok.Data;

@Data
public class Product {
	 private Integer id;
	 private String ean;
	 private Integer hold;
	 private String redistributable;
	 private String sku;
	 private Integer stock;
	 private Integer shop_id;
	 private Integer id_fulfillment;
	 private Boolean is_fulfillment; 
	 private String nombre; 
	 private Integer t1_storeid;
	
}
