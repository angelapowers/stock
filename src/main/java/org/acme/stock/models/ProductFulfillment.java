package org.acme.stock.models;

import lombok.Data;

@Data
public class ProductFulfillment {

	private String status;
	private Integer t1_storeid;
	private String sku;
	private String ean;
	private Integer id_fulfillment;

	public ProductFulfillment setValues(ProductFulfillment change ,FulfillmentRequest value) {
		change.setEan(value.getEan()); 
		change.setId_fulfillment(value.getId_fulfillment()); 
		change.setSku(value.getSku());
		change.setT1_storeid(value.getT1_storeid());
		change.setStatus("Transaccion exitosa");		
		return change; 
	}
	
}
