package org.acme.stock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "products")
@Data 
public class Products {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Column(name="id")
	 private Integer id;
	 @Column(name="ean")
	 private String ean;
	 @Column(name="hold")
	 private Integer hold;
	 @Column(name="redistributable")
	 private String redistributable;
	 @Column(name="sku")
	 private String sku;
	 @Column(name="stock")
	 private Integer stock;
	 @Column(name="shop_id")
	 private Integer shop_id;
	 @Column(name="id_fulfillment")
	 private Integer id_fulfillment;
	 @Column(name="is_fulfillment")
	 private Boolean is_fulfillment; 
	 @Column(name="nombre")
	 private String nombre; 
	 @Column(name="t1_storeid")
	 private Integer t1_storeid;
	

}
