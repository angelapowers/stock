package org.acme.stock.models;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class SetFulfillmentRequest {

	private Integer id_fulfillment;
	private String acction;
	
}
