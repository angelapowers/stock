package org.acme.stock.models;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class SetFulfillmentResponse {
		
	private	String sku;
	private	String ean;
	private	Integer t1_storeid;
	private Integer id_fulfillment;
	private String accion;	
	
	
}
