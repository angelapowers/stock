package org.acme.stock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.smallrye.common.constraint.NotNull;
import lombok.Data;

@Data
@Entity
@Table(name = "shops")
public class Shop {
	
	@NotNull
	@Column(name="name")
	private String name;
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id_cs;
	@NotNull
	@Column(name="t1_storeid")
	private Integer t1_storeid;
	@NotNull
    @Column(name="company")
	private Integer company;
}
