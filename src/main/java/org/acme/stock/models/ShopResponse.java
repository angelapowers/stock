package org.acme.stock.models;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;


@RegisterForReflection
@Data
public class ShopResponse  {
	
	private String name;
	private Integer id_cs;
	private Integer t1_storeid;
	private Integer company;

}
