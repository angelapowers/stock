package org.acme.stock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "subsidiary_stock")
@Data
public class SubsidiaryStock {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id ;
	@Column(name="stock")
	private Integer stock; 
	@Column(name="product_id")
	private Integer product_id;
	@Column(name="subsidiary_id")
	private Long subsidiary_id;

}
