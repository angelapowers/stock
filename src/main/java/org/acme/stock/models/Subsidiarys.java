package org.acme.stock.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.smallrye.common.constraint.NotNull;
import lombok.Data;

@Entity
@Table(name = "subsidiarys")
@Data
public class Subsidiarys {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@NotNull
	@Column(name="name")
	private String name;
	@NotNull
	@Column(name="subsidary_external_id")
	private Integer subsidary_external_id;  
	@NotNull
	@Column(name="shop_id")
	private Integer shop_id; 
	@NotNull
	@Column(name="cyr")
	private Boolean cyr;
	@NotNull
	@Column(name="complete_Address")
	private String complete_Address;
	@NotNull
	@Column(name="schedule")
	private String schedule;
	@NotNull
	@Column(name="phone")
	private String phone;

}
