package org.acme.stock.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.acme.stock.models.Products;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ProductRepository implements PanacheRepository<Products> {
	
	public Products findById_fulfillment(Integer id_fulfillment){
        return find("id_fulfillment", id_fulfillment).firstResult();
    }
	
	public Products findBySku(String sku){
        return find("sku", sku).firstResult();
    }
	
	public List<Products> findByShop_id(Integer shop_id){
        return list("shop_id", shop_id);
    }

}
