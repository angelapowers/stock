package org.acme.stock.repository;

import javax.enterprise.context.ApplicationScoped;

import org.acme.stock.models.Shop;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ShopRepository implements PanacheRepository<Shop> {
	
	public PanacheQuery<Shop> shopPagination() {
		PanacheQuery<Shop> shopPage = findAll();
		return shopPage;  
	}
	
	public Shop findByT1_storeid(Integer t1_storeid){
        return find("t1_storeid", t1_storeid).firstResult();
    }
	
	

}
