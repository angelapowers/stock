package org.acme.stock.repository;

import javax.enterprise.context.ApplicationScoped;

import org.acme.stock.models.SubsidiaryStock;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class SubsidiaryStockRepository implements PanacheRepository<SubsidiaryStock>{

	
	public SubsidiaryStock findByProduct_id(Integer product_id){
        return find("product_id", product_id).firstResult();
    }
	
	public SubsidiaryStock findBySubsidary_id(Integer subsidary_id){
        return find("product_id", subsidary_id).firstResult();
    }
	
	public SubsidiaryStock findBySubsidary_idAndProduct_id(Integer product_id, Long subsidary_id){
		return find( "subsidiary_id =?1 and product_id =?2 ", subsidary_id , product_id).firstResult();
    }
}
