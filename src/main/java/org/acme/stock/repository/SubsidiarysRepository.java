package org.acme.stock.repository;

import javax.enterprise.context.ApplicationScoped;


import org.acme.stock.models.Subsidiarys;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class SubsidiarysRepository implements PanacheRepository<Subsidiarys> {
	
	public Subsidiarys findByShop_id(Integer shop_id){
        return find("shop_id", shop_id).firstResult();
    }
	
}
