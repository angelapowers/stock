package org.acme.stock.schema;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.Shop;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class CreateShop {
	
	private BasicMetaData metadata; 
	private Shop data; 
 	
}
