package org.acme.stock.schema;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.SetFulfillmentResponse;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class SchemSetFul {

	private BasicMetaData metadata; 
	private SetFulfillmentResponse data;

}
