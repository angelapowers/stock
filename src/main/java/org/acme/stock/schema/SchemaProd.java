package org.acme.stock.schema;

import java.util.List;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.ListProductos;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class SchemaProd {

	private BasicMetaData metadata; 
	private List<ListProductos> data; 
}
