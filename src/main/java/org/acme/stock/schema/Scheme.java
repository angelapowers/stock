package org.acme.stock.schema;

import java.util.List;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.PaginationMetadata;
import org.acme.stock.models.Shop;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class Scheme {
	private BasicMetaData metadata; 
	private List<Shop> data; 
	private PaginationMetadata pagination;
}
