package org.acme.stock.schema;

import java.util.List;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.ProductFulfillment;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@RegisterForReflection
@Data
public class SchemeFullAdd {
	
	private BasicMetaData metadata; 
	private List<ProductFulfillment> data;

}
