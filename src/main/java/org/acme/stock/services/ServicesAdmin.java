package org.acme.stock.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.PaginationMetadata;
import org.acme.stock.models.Shop;
import org.acme.stock.models.ShopResponse;
import org.acme.stock.repository.ProductRepository;
import org.acme.stock.repository.ShopRepository;
import org.acme.stock.schema.CreateShop;
import org.acme.stock.schema.Scheme;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.smallrye.common.constraint.NotNull;

@ApplicationScoped
public class ServicesAdmin {
	
	@Inject
	ShopRepository shopRepository;
	
	@Inject
	ProductRepository productRepository;
	
	
	public Response getShopList(@QueryParam("pageSize") Integer pageSize){
		PanacheQuery<Shop> data = shopRepository.shopPagination();
		try { 
			PaginationMetadata pagination = new PaginationMetadata();
			BasicMetaData metadata = new BasicMetaData();
			List<Shop> listData =new ArrayList<Shop>();
			if(Objects.isNull(pageSize)) {
				data.page(Page.ofSize(15));
				listData = data.list();
				pagination.setTotal_count(data.count());
				pagination.setPage_size(15);
				pagination.setPage(data.pageCount());
				metadata.InitialValue(metadata,Objects.isNull(listData));
			}else {
				data.page(Page.ofSize(pageSize));
				listData = data.list();
				pagination.setTotal_count(data.count());
				pagination.setPage_size(pageSize);
				pagination.setPage(data.pageCount());
				metadata.InitialValue(metadata,Objects.isNull(listData));
			}
			
			Scheme getList = new Scheme();
			getList.setMetadata(metadata);
			getList.setData(listData);
			getList.setPagination(pagination);
			return Response.ok(getList).build();
		  }catch(Exception e){
			  System.out.println("Error " + e.toString() );
			  Map<String, String> mensaje = new HashMap <String,String>(); 
			  mensaje.put("Error","conexion");
			  return Response.ok(mensaje).build();
			}
	}
	
	@Transactional
	public Response addShop(@NotNull ShopResponse add) { 
		try{
			Shop addShop = new Shop() ;
			addShop.setCompany(add.getCompany());
			addShop.setId_cs(add.getId_cs());
			addShop.setName(add.getName());
			addShop.setT1_storeid(add.getT1_storeid());
			shopRepository.persist(addShop);
			BasicMetaData metadata = new BasicMetaData ();
			metadata.InitialValue(metadata,Objects.isNull(addShop));
			CreateShop shop = new CreateShop();
			shop.setMetadata(metadata);
			shop.setData(addShop);
			return Response.ok(shop).build(); 
		}catch(Exception e){
			System.out.println("Error " + e.toString() );
			Map<String, String> mensaje = new HashMap <String,String>(); 
			mensaje.put("Error","conexion");
			return Response.ok(mensaje).build();
		}
	}
	
	public Response findStoreid (Integer id) {
		Shop getShop = shopRepository.findByT1_storeid(id);
		try {
			if(getShop == null){
				throw new WebApplicationException("PRODUCT WITH THIS ID DOES NOT EXIST"); 
			}else {
			BasicMetaData metadata = new BasicMetaData ();
			metadata.InitialValue(metadata,Objects.isNull(getShop));
			CreateShop shop = new CreateShop();
			shop.setData(getShop);
			shop.setMetadata(metadata);
			return Response.ok(shop).build();
			}
		}catch(Exception e){
			System.out.println("Error " + e.toString() );
			Map<String, String> mensaje = new HashMap <String,String>(); 
			mensaje.put("Error","conexion");
			return Response.ok(mensaje).build();
		}
	}
	
	@Transactional
	public Response updateStoreid (Integer id,@NotNull ShopResponse updata) {
		try {
			Shop upShop = shopRepository.findByT1_storeid(id);
			if(upShop == null){
				throw new WebApplicationException("PRODUCT WITH THIS ID DOES NOT EXIST"); 
			}else {
				upShop.setCompany(updata.getCompany());
				upShop.setName(updata.getName());
				upShop.setT1_storeid(updata.getT1_storeid());
			}
			BasicMetaData metadata = new BasicMetaData ();
			metadata.InitialValue(metadata,Objects.isNull(upShop));
			CreateShop shop = new CreateShop();
			shop.setData(upShop);
			shop.setMetadata(metadata);
			return Response.ok(shop).build(); 
		}catch(Exception e){
			System.out.println("Error " + e.toString() );
			Map<String, String> mensaje = new HashMap <String,String>(); 
			mensaje.put("Error","conexion");
			return Response.ok(mensaje).build();
		}
	}

	@Transactional
	public Response upPatchStore (Integer id, Map<String, Object> fields) {
		Shop upShop = shopRepository.findByT1_storeid(id);
		try {
			if(upShop == null){
				throw new WebApplicationException("PRODUCT WITH THIS ID DOES NOT EXIST"); 
			}else {
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> propiedades = mapper.convertValue(upShop, Map.class);
				for(String k :propiedades.keySet()) {
					for(String a:fields.keySet()) {
						if (k.equals(a)) {
							propiedades.put(k,fields.get(k));
							ShopResponse cont =  mapper.convertValue(propiedades, ShopResponse.class);
							upShop.setCompany(cont.getCompany());
							upShop.setName(cont.getName());
							upShop.setT1_storeid(cont.getT1_storeid());
						}
					}
				}
			}
			BasicMetaData metadata = new BasicMetaData ();
			metadata.InitialValue(metadata,Objects.isNull(upShop));
			CreateShop shop = new CreateShop();
			shop.setData(upShop);
			shop.setMetadata(metadata);
			return Response.ok(shop).build();
		}catch(Exception e){
			System.out.println("Error " + e.toString() );
			Map<String, String> mensaje = new HashMap <String,String>(); 
			mensaje.put("Error","conexion");
			return Response.ok(mensaje).build();
		}
	}
	
	
}
