package org.acme.stock.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.ListProductRequest;
import org.acme.stock.models.ListProductos;
import org.acme.stock.models.Products;
import org.acme.stock.models.Shop;
import org.acme.stock.repository.ProductRepository;
import org.acme.stock.repository.ShopRepository;
import org.acme.stock.schema.SchemaProd;

@ApplicationScoped
public class ServicesFront {

	@Inject
	ShopRepository shopRepository;
	
	@Inject
	ProductRepository productRepository;
	
	@Transactional
	public Response getProductSku (ListProductRequest lookfor) {
		List<ListProductos> newList = new ArrayList<ListProductos> ();  
		try { 
			Shop shop =shopRepository.findByT1_storeid(lookfor.getT1_storeid());
			if(shop == null) {
				throw new WebApplicationException("PRODUCT WITH THIS ID DOES NOT EXIST");
			}
			List<Products> list = productRepository.findByShop_id(shop.getId_cs());
			for(Products p: list) {
				for(String v :lookfor.getSku()) {
					if(p.getSku().equals(v)) {
						ListProductos cont = new ListProductos();
						cont.initValues(cont, p);
						newList.add(cont);
					}
				}
			}
			BasicMetaData metadata = new BasicMetaData ();
			metadata.InitialValue(metadata,newList.isEmpty());
			SchemaProd scheme = new SchemaProd();
			scheme.setMetadata(metadata);
			scheme.setData(newList);
			return Response.ok(scheme).build();
		}catch(Exception e){
			e.printStackTrace();
			Map<String, String> mensaje = new HashMap <String,String>(); 
			mensaje.put("Error","conexion");
			return Response.ok(mensaje).build();
		}
	}
}
