package org.acme.stock.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.acme.stock.models.BasicMetaData;
import org.acme.stock.models.FulfillmentRequest;
import org.acme.stock.models.ProductFulfillment;
import org.acme.stock.models.Products;
import org.acme.stock.models.SetFulfillmentRequest;
import org.acme.stock.models.SetFulfillmentResponse;
import org.acme.stock.models.Shop;
import org.acme.stock.models.SubsidiaryStock;
import org.acme.stock.models.Subsidiarys;
import org.acme.stock.repository.ProductRepository;
import org.acme.stock.repository.ShopRepository;
import org.acme.stock.repository.SubsidiaryStockRepository;
import org.acme.stock.repository.SubsidiarysRepository;
import org.acme.stock.schema.SchemSetFul;
import org.acme.stock.schema.SchemeFullAdd;

@ApplicationScoped
public class ServicesFulFilment {
	
	@Inject
	SubsidiaryStockRepository subsidiaryStockRepository;
	
	@Inject
	SubsidiarysRepository subsidiarysRepository;
	
	@Inject
	ShopRepository shopRepository;
	
	@Inject
	ProductRepository productRepository;
	
	@Transactional
	public Response addFullfilment (List<FulfillmentRequest> add) {
		List<ProductFulfillment> listProd = new ArrayList<ProductFulfillment>();
		try {
			for(FulfillmentRequest value: add) {
				Products exist = productRepository.findBySku(value.getSku());
				Shop shop = shopRepository.findByT1_storeid(value.getT1_storeid()); 
				if(Objects.isNull(exist) == false && exist.getIs_fulfillment()== false) {
					exist.setEan(value.getEan());
					exist.setIs_fulfillment(true);
					exist.setId_fulfillment(value.getId_fulfillment());
					exist.setStock(0);
					Subsidiarys existSub = subsidiarysRepository.findByShop_id(shop.getId_cs());
					Subsidiarys subsidary = new Subsidiarys();
					if(Objects.isNull(existSub)) {
						subsidary.setShop_id(shop.getId_cs());
						subsidary.setSubsidary_external_id(2);
						subsidiarysRepository.persist(subsidary);
						SubsidiaryStock valid = subsidiaryStockRepository.findBySubsidary_idAndProduct_id(exist.getId(),subsidary.getId());
						if(Objects.isNull(valid)) {
							createSubStock(exist,subsidary);
						}
					}else {
						SubsidiaryStock valid = subsidiaryStockRepository.findBySubsidary_idAndProduct_id(exist.getId(),existSub.getId());
						if(Objects.isNull(valid)) {
							createSubStock(exist,existSub);
						}
					}
					ProductFulfillment item = new ProductFulfillment();
					item.setValues(item, value); 
					listProd.add(item);
				}else {
					Products nprodFull = new Products();
					nprodFull.setId_fulfillment(value.getId_fulfillment());
					nprodFull.setIs_fulfillment(true);
					nprodFull.setEan(value.getEan());
					nprodFull.setT1_storeid(value.getT1_storeid());
					nprodFull.setStock(0);
					nprodFull.setHold(0);
					nprodFull.setNombre("producto prueba");
					nprodFull.setShop_id(shop.getId_cs());
					nprodFull.setSku(value.getSku()); 
					productRepository.persist(nprodFull);
					Subsidiarys existSub = subsidiarysRepository.findByShop_id(shop.getId_cs());
					Subsidiarys subsidary = new Subsidiarys();
					if(Objects.isNull(existSub)) {
						subsidary.setShop_id(shop.getId_cs());
						subsidary.setSubsidary_external_id(2);
						subsidiarysRepository.persist(subsidary);
						SubsidiaryStock valid = subsidiaryStockRepository.findBySubsidary_idAndProduct_id(nprodFull.getId(),subsidary.getId());
						if(Objects.isNull(valid)) {
							createSubStock(nprodFull,subsidary);
						}
					}else {
						SubsidiaryStock valid = subsidiaryStockRepository.findBySubsidary_idAndProduct_id(nprodFull.getId(),existSub.getId());
						if(Objects.isNull(valid)) {
							createSubStock(nprodFull,existSub);
						}
					}
				}
				ProductFulfillment item = new ProductFulfillment();
				item.setValues(item, value);
				listProd.add(item);
		}
			SchemeFullAdd getscheme = new SchemeFullAdd(); 
			BasicMetaData metadata = new BasicMetaData ();
			metadata.InitialValue(metadata, listProd.isEmpty());
			getscheme.setData(listProd);
			getscheme.setMetadata(metadata);
			return Response.ok(getscheme).build();
		}catch(Exception e) {
			e.printStackTrace();
			Map<String, String> mensaje = new HashMap <String,String>(); 
			mensaje.put("Error","conexion");
			return Response.ok(mensaje).build();
		}
	}
	
	@Transactional
	public Response setFulfillment (SetFulfillmentRequest set) {
		SchemSetFul schema = new SchemSetFul ();
		SetFulfillmentResponse response = new SetFulfillmentResponse();
		try {
			Products product = productRepository.findById_fulfillment(set.getId_fulfillment());
			if(product == null) {
				throw new WebApplicationException("THE ID FULFILLMENT DOES NOT EXIST");
			}
			if(set.getAcction().equals("TO-FULFILLMENT")) {
				if(product.getIs_fulfillment() == false) {
					product.setIs_fulfillment(true);
					product.setStock(0);
					product.setHold(0);
					response.setAccion("TO-DROPSHIPPING  | TO-FULFILLMENT");
					SubsidiaryStock changeStock = subsidiaryStockRepository.findByProduct_id(product.getId());
					Subsidiarys changeSub = subsidiarysRepository.findByShop_id(product.getShop_id());
					if(Objects.isNull(changeStock) == false && changeSub.getSubsidary_external_id() == 1) {
						changeStock.setStock(0);
						changeSub.setSubsidary_external_id(2);
					}	
				}else {
					response.setAccion("PRODUCT IS FULFILLMENT");
				}
				response.setEan(product.getEan());
				response.setId_fulfillment(product.getId_fulfillment());
				response.setSku(product.getSku());
				response.setT1_storeid(product.getT1_storeid());
				BasicMetaData metadata = new BasicMetaData(); 
				metadata.InitialValue(metadata,Objects.isNull(response));
				schema.setMetadata(metadata);
				schema.setData(response);
			}else if(set.getAcction().equals("TO-DROPSHIPPING")  ) {
				if(product.getIs_fulfillment() == true) {
					product.setIs_fulfillment(false);
					product.setStock(0);
					product.setHold(0);
					response.setAccion("TO-FULFILLMENT | TO-DROPSHIPPING");
					SubsidiaryStock changeStock = subsidiaryStockRepository.findByProduct_id(product.getId());
					Subsidiarys changeSub = subsidiarysRepository.findByShop_id(product.getShop_id());
					if(Objects.isNull(changeStock) == false && changeSub.getSubsidary_external_id() == 2) {
						changeStock.setStock(0);
						changeSub.setSubsidary_external_id(1);
					}
				}else {
					response.setAccion("PRODUCT IS DROPSHIPPING");
				}
				BasicMetaData metadata = new BasicMetaData();  
				response.setEan(product.getEan());
				response.setId_fulfillment(product.getId_fulfillment());
				response.setSku(product.getSku());
				response.setT1_storeid(product.getT1_storeid());
				metadata.InitialValue(metadata,Objects.isNull(response));
				schema.setMetadata(metadata);
				schema.setData(response);
			}
			return Response.ok(schema).build();
			} catch(Exception e) {
				e.printStackTrace();
				Map<String, String> mensaje = new HashMap <String,String>(); 
				mensaje.put("Error","conexion");
				return Response.ok(mensaje).build();
			}
		} 
	
	public void createSubStock (Products product, Subsidiarys subsidary) {
		SubsidiaryStock subStock = new SubsidiaryStock(); 
		subStock.setSubsidiary_id(subsidary.getId());
		subStock.setStock(0);
		subStock.setProduct_id(product.getId());
		subsidiaryStockRepository.persist(subStock);
	}
	
}



